import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateSlang extends JFrame {

	private JPanel contentPane;
	private JTextField tfSlang;
	private JTextField tfDefintion;
	public String strSlangUpdate;
	public String strDefintion;
	
	/**
	 * Launch the application.
	 */
	public static void main(String strSlang, String strValue) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateSlang frame = new UpdateSlang(strSlang, strValue);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void closeFrame() {
		this.dispose();
	}
	
	/**
	 * Create the frame.
	 */
	public UpdateSlang(String strSlang, String strValue) {
		setTitle("Update Slang");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 555, 206);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Slang");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(27, 27, 102, 24);
		contentPane.add(lblNewLabel);
		
		tfSlang = new JTextField();
		tfSlang.setBounds(94, 29, 375, 23);
		contentPane.add(tfSlang);
		tfSlang.setColumns(10);
		tfSlang.setText(strSlang);
		
		JLabel lblNewLabel_1 = new JLabel("Defintion");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(27, 81, 65, 13);
		contentPane.add(lblNewLabel_1);
		
		tfDefintion = new JTextField();
		tfDefintion.setBounds(94, 74, 375, 24);
		contentPane.add(tfDefintion);
		tfDefintion.setColumns(10);
		tfDefintion.setText(strValue);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strSlangUpdate = tfSlang.getText();
				strDefintion = tfDefintion.getText();
				if (strSlangUpdate == null || strSlangUpdate.equals("") || strDefintion == null || strDefintion.equals("")) {
					JOptionPane.showMessageDialog(null, "Enter Slang or Defintion");
					return;
				}
				main.updateSlang(strSlangUpdate, strDefintion);
				closeFrame();
			}
		});
		btnUpdate.setBounds(215, 114, 85, 29);
		contentPane.add(btnUpdate);
	}

}
