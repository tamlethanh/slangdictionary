import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddSlang extends JFrame {

	private JPanel contentPane;
	private JTextField tfSlang;
	private JTextField tfDefintion;
	public String strSlang;
	public String strDefintion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddSlang frame = new AddSlang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void closeFrame() {
		this.dispose();
	}
	
	/**
	 * Create the frame.
	 */
	public AddSlang() {
		setTitle("Add Slang");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 555, 206);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Slang");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(27, 27, 102, 24);
		contentPane.add(lblNewLabel);
		
		tfSlang = new JTextField();
		tfSlang.setBounds(94, 29, 375, 23);
		contentPane.add(tfSlang);
		tfSlang.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Defintion");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(27, 81, 65, 13);
		contentPane.add(lblNewLabel_1);
		
		tfDefintion = new JTextField();
		tfDefintion.setBounds(94, 74, 375, 24);
		contentPane.add(tfDefintion);
		tfDefintion.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				strSlang = tfSlang.getText();
				strDefintion = tfDefintion.getText();
				if (strSlang == null || strSlang.equals("") || strDefintion == null || strDefintion.equals("")) {
					JOptionPane.showMessageDialog(null, "Enter Slang or Defintion");
					return;
				}
				main.addNewSlang(strSlang, strDefintion);
				closeFrame();
				
			}
		});
		btnAdd.setBounds(215, 114, 85, 29);
		contentPane.add(btnAdd);
	}
}
