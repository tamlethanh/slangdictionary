import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.SwingConstants;
import javax.swing.JList;

public class history extends JFrame {

	private JPanel contentPane;
	private JList<String> lstHistory;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					history frame = new history();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	public static void main(ArrayList<String> lstHistory) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					history frame = new history(lstHistory);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
		
	/**
	 * Create the frame.
	 * @param lstHistory2 
	 */
	public history(ArrayList<String> lstHistory2) {
		setTitle("L\u1ECBch s\u1EED t\u00ECm ki\u1EBFm");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 859, 709);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("L\u1ECBch s\u1EED t\u00ECm ki\u1EBFm");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(27, 10, 808, 32);
		contentPane.add(lblNewLabel);
		
		DefaultListModel<String> model = new DefaultListModel<>();
		lstHistory = new JList<>( model );
		lstHistory.setBounds(10, 98, 825, 564);
		for (int i = 0; i < lstHistory2.size(); i++) {
			model.addElement(lstHistory2.get(i));
		}
		contentPane.add(lstHistory);
	}

}
