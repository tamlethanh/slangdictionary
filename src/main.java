import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class main {

	private JFrame frmSlangDictionary;
	private JTable tbDictionary;
	private JTextField txtSearch;
	public static LinkedHashMap<String, SlangModel> hmDictionary = new LinkedHashMap<String, SlangModel>();
	private JRadioButton rdTypeSearchKey;
	private JRadioButton rbTypeSearchValue;
	private ArrayList<String> lstHistory = new ArrayList<String>();
	private static Object[] optConfirmBtn = { "Overwrite", "Duplicate", "Cancel" };
	private static DefaultTableModel modelTb;
	int TYPE_GAME_SLANG = 0;
	int TYPE_GAME_DEFINITION = 1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frmSlangDictionary.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}
	
	public void onGetDataDictionary() {
		String linkFile = ".\\src\\slang.txt";
		File tmpFile = new File(linkFile);
		if (!tmpFile.exists()) {
			JOptionPane.showMessageDialog(null, "File not exist");
			return;
		}
		try {
			FileInputStream fis = new FileInputStream(tmpFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String arrStr[];
			String itemStr;
			while (br.ready()) {
				itemStr = br.readLine();
				arrStr = itemStr.split("`");
				if (arrStr.length != 2) {
					continue;
				}
				if (!hmDictionary.containsKey(arrStr[0])) {
					SlangModel item = new SlangModel(arrStr[0], arrStr[1], false);
					hmDictionary.put(arrStr[0], item);
				}
			}
			br.close();
			fis.close();
		}
		catch(Exception error) {
			System.out.print(error);
		}
	}
	
	public static Boolean checkSlangExist(String strKey) {
		return hmDictionary.containsKey(strKey);
	}
	
	public static void addNewSlang(String strKey, String strValue) {
		if (checkSlangExist(strKey)) {
			int result = JOptionPane.showOptionDialog(null, "Slang is exist", "Warning",
				    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, 
				    optConfirmBtn, null);
			
			if (result == JOptionPane.YES_OPTION) {
				SlangModel item = new SlangModel(strKey, strValue, false);
				hmDictionary.put(strKey, item);
			} else if (result == JOptionPane.NO_OPTION) {
				SlangModel itemOld = hmDictionary.get(strKey);
				String newValue = itemOld.getDefintion() + " | " + strValue;
				itemOld.setDefintion(newValue);
				hmDictionary.put(strKey, itemOld);
			} else {
				return;
			}			
		} else {
			SlangModel item = new SlangModel(strKey, strValue, true);
			hmDictionary.put(strKey, item);
		}
		loadDataTable(modelTb);
	}
	
	public static void updateSlang(String strKey, String strValue) {
		if (checkSlangExist(strKey)) {
			SlangModel itemOld = hmDictionary.get(strKey);
			SlangModel item = new SlangModel(strKey, strValue, itemOld.getIsNewSlang());
			hmDictionary.put(strKey, item);
		} else {
			SlangModel item = new SlangModel(strKey, strValue, true);
			hmDictionary.put(strKey, item);
		}
		loadDataTable(modelTb);
	}
	
	
	public void saveFile() {
		String linkFile = ".\\src\\slang.txt";
		File tmpFile = new File(linkFile);
		try {
			FileWriter fw = new FileWriter(tmpFile);
	        BufferedWriter bw = new BufferedWriter(fw);
	        int i = 0;
	        for (Map.Entry<String, SlangModel> e: hmDictionary.entrySet()) {
	        	bw.write(e.getKey()+"`"+e.getValue().getDefintion());
	        	if (i < hmDictionary.size() - 1) {
	        		bw.newLine();
	        		i++;
	        	}
	        }
	        bw.close();
	        fw.close();
	        
		}
		catch(Exception error) {
			System.out.print(error);
		}
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSlangDictionary = new JFrame();
		frmSlangDictionary.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				saveFile();
			}
		});
		frmSlangDictionary.setTitle("Slang Dictionary");
		frmSlangDictionary.setBounds(100, 100, 967, 717);
		frmSlangDictionary.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSlangDictionary.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 113, 933, 535);
		frmSlangDictionary.getContentPane().add(scrollPane);
		
		String arrColumn[] = {"Slang", "Defintion"};
		
		onGetDataDictionary();
		System.out.print(hmDictionary.size());
		modelTb = new DefaultTableModel(0, 0);
		modelTb.setColumnIdentifiers(arrColumn);
		
		tbDictionary = new JTable();
		tbDictionary.setModel(modelTb);
		tbDictionary.getTableHeader().setReorderingAllowed(false);
		loadDataTable(modelTb);
		scrollPane.setViewportView(tbDictionary);
		
		JLabel lbSearch = new JLabel("Nh\u1EADp t\u1EEB kh\u00F3a");
		lbSearch.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbSearch.setBounds(10, 10, 94, 30);
		frmSlangDictionary.getContentPane().add(lbSearch);
		
		txtSearch = new JTextField();
		txtSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strSearch = txtSearch.getText();
				if (strSearch == "") {
					loadDataTable(modelTb);
				} else if (rdTypeSearchKey.isSelected()) {
					lstHistory.add(strSearch);
					searchKey(modelTb, strSearch);
				} else if (rbTypeSearchValue.isSelected()) {
					searchValue(modelTb, strSearch);
				}
			}
		});
		txtSearch.setToolTipText("Nh\u1EA5p Enter \u0111\u1EC3 search");
		txtSearch.setBounds(101, 12, 842, 30);
		frmSlangDictionary.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);
		
		rdTypeSearchKey = new JRadioButton("T\u00ECm theo t\u1EEB kh\u00F3a");
		rdTypeSearchKey.setSelected(true);
		rdTypeSearchKey.setBounds(10, 63, 124, 21);
		frmSlangDictionary.getContentPane().add(rdTypeSearchKey);
		
		rbTypeSearchValue = new JRadioButton("T\u00ECm theo ngh\u0129a");
		rbTypeSearchValue.setBounds(158, 63, 124, 21);
		frmSlangDictionary.getContentPane().add(rbTypeSearchValue);
		
		ButtonGroup btnGroup = new ButtonGroup();
		btnGroup.add(rdTypeSearchKey);
		btnGroup.add(rbTypeSearchValue);
		
		JMenuBar menuBar = new JMenuBar();
		frmSlangDictionary.setJMenuBar(menuBar);
		
		JMenu mnFunction = new JMenu("Ch\u1EE9c n\u0103ng");
		menuBar.add(mnFunction);
		
		JMenuItem mnAdd = new JMenuItem("Th\u00EAm");
		mnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddSlang addPopup = new AddSlang();
				addPopup.setVisible(true);
				
			}
		});
		mnFunction.add(mnAdd);
		
		JMenuItem mnUpdate = new JMenuItem("C\u1EADp nh\u1EADt");
		mnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selection = tbDictionary.getSelectedRow();
				if (selection == -1) {
					JOptionPane.showMessageDialog(null, "Select record");
					return;
				}
				Vector<Object> rowData = modelTb.getDataVector().elementAt(selection);
				String strKey = (String) rowData.get(0);
				String strValue = (String) rowData.get(1);
				UpdateSlang updatePopup = new UpdateSlang(strKey, strValue);
				updatePopup.setVisible(true);
			}
		});
		mnFunction.add(mnUpdate);
		
		JMenuItem mnDelete = new JMenuItem("X\u00F3a");
		mnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selection = tbDictionary.getSelectedRow();
				if (selection == -1) {
					JOptionPane.showMessageDialog(null, "Select record");
					return;
				}
				
				int result = JOptionPane.showOptionDialog(null, "Are you sure delete?", "Warning",
					    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				if (result == JOptionPane.YES_OPTION) {
					Vector<Object> rowData = modelTb.getDataVector().elementAt(selection);
					String strKey = (String) rowData.get(0);
					hmDictionary.remove(strKey);
				} else {
					return;
				}
				loadDataTable(modelTb);
			}
		});
		mnFunction.add(mnDelete);
		
		JMenu mnUlti = new JMenu("Ti\u1EC7n \u00EDch");
		menuBar.add(mnUlti);
		
		JMenuItem mnHistory = new JMenuItem("L\u1ECBch s\u1EED t\u00ECm ki\u1EBFm");
		mnHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				history hstFrame = new history(lstHistory);
				hstFrame.setVisible(true);
			}
		});
		mnUlti.add(mnHistory);
		
		JMenuItem mnReset = new JMenuItem("Kh\u00F4i ph\u1EE5c ban \u0111\u1EA7u");
		mnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset(modelTb);
			}
		});
		mnUlti.add(mnReset);
		
		JMenuItem mnRandom = new JMenuItem("Random");
		mnRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Set<String> arrStrKey = hmDictionary.keySet();
				int itemKey = new Random().nextInt(arrStrKey.size());
				ArrayList<String> lstStrkey = new ArrayList<String>(arrStrKey);
				String itemIndex = lstStrkey.get(itemKey);
				SlangModel strDefinition = hmDictionary.get(itemIndex);				
				JOptionPane.showMessageDialog(null, "Slang: " + itemIndex + "\n" + "Definition: " + strDefinition.getDefintion());
			}
		});
		mnUlti.add(mnRandom);
		
		JMenu mnNewMenu = new JMenu("Tr\u00F2 ch\u01A1i");
		menuBar.add(mnNewMenu);
		
		JMenuItem mnShuffleSlang = new JMenuItem("Tr\u00F2 ch\u01A1i 1");
		mnShuffleSlang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PopupGame popGame = new PopupGame(hmDictionary, TYPE_GAME_SLANG);
				popGame.setVisible(true);
			}
		});
		mnNewMenu.add(mnShuffleSlang);
		JMenuItem mnShuffleDefinition = new JMenuItem("Tr\u00F2 ch\u01A1i 2");
		mnShuffleDefinition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PopupGame popGame = new PopupGame(hmDictionary, TYPE_GAME_DEFINITION);
				popGame.setVisible(true);
			}
		});
		mnNewMenu.add(mnShuffleDefinition);
	}
	
	public static void loadDataTable(DefaultTableModel tbModel) {
		tbModel.setRowCount(0);
		for (Entry<String, SlangModel> e: hmDictionary.entrySet()) {
			Object[] data = {e.getKey(), e.getValue().getDefintion() };
			tbModel.addRow(data);
		}
	}
	
	public void searchKey(DefaultTableModel tbModel, String strSearch) {
		tbModel.setRowCount(0);
		for (Map.Entry<String, SlangModel> e: hmDictionary.entrySet()) {
			if(e.getKey().toLowerCase().startsWith(strSearch.toLowerCase())){
				Object[] data = {e.getKey(), e.getValue().getDefintion() };
				tbModel.addRow(data);
			}
		}
	}
	
	public void searchValue(DefaultTableModel tbModel, String strSearch) {
		tbModel.setRowCount(0);
		for (Map.Entry<String, SlangModel> e: hmDictionary.entrySet()) {
			if (e.getValue().getDefintion().toLowerCase().startsWith(strSearch.toLowerCase())) {
				Object[] data = {e.getKey(), e.getValue().getDefintion() };
				tbModel.addRow(data);
			}
		}
	}
	
	public void reset(DefaultTableModel tbModel) {
		tbModel.setRowCount(0);
		for (Map.Entry<String, SlangModel> e: hmDictionary.entrySet()) {
			if (!e.getValue().getIsNewSlang()) {
				Object[] data = {e.getKey(), e.getValue().getDefintion() };
				tbModel.addRow(data);
			}
		}
	}
}
