
public class SlangModel {
	private String slang;
	private String defintion;
	private Boolean isNewSlang;
	public SlangModel(String strSlang, String strDefintion, Boolean isNew) {
		this.slang = strSlang;
		this.defintion = strDefintion;
		this.isNewSlang = isNew;
	}
	
	public String getSlang() {
		return slang;
	}
	public void setSlang(String slang) {
		this.slang = slang;
	}
	public String getDefintion() {
		return defintion;
	}
	public void setDefintion(String defintion) {
		this.defintion = defintion;
	}

	public Boolean getIsNewSlang() {
		return isNewSlang;
	}

	public void setIsNewSlang(Boolean isNewSlang) {
		this.isNewSlang = isNewSlang;
	}
}
