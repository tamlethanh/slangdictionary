import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PopupGame extends JFrame {

	private JPanel contentPane;
	private String answerCorrect;
	private ButtonGroup btnGroup;

	/**
	 * Launch the application.
	 */
	public static void main(LinkedHashMap<String, SlangModel> hmDictionary, int typeGame) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupGame frame = new PopupGame(hmDictionary, typeGame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public PopupGame(LinkedHashMap<String, SlangModel> hmDictionary, int typeGame) {
		setTitle("Game");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 540, 405);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		JLabel lblQuestion = new JLabel("");
		lblQuestion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblQuestion.setBounds(10, 40, 504, 22);
		contentPane.add(lblQuestion);
		
		JLabel lblNewLabel = new JLabel("Choose the correct answer");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(10, 70, 504, 34);
		contentPane.add(lblNewLabel);
		
		JRadioButton rdAnswerA = new JRadioButton("");
		rdAnswerA.setBounds(6, 120, 508, 30);
		contentPane.add(rdAnswerA);
		
		JRadioButton rdAnswerB = new JRadioButton("");
		rdAnswerB.setBounds(6, 170, 512, 30);
		contentPane.add(rdAnswerB);
		
		JRadioButton rdAnswerC = new JRadioButton("");
		rdAnswerC.setBounds(6, 220, 516, 30);
		contentPane.add(rdAnswerC);
		
		JRadioButton rdAnswerD = new JRadioButton("");
		rdAnswerD.setBounds(6, 270, 508, 30);
		contentPane.add(rdAnswerD);
		
		JButton btnCheck = new JButton("Check");
		btnCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String chooseAnswer = btnGroup.getSelection().getActionCommand();
				if (chooseAnswer == answerCorrect) {
					JOptionPane.showMessageDialog(null, "Congratulations");
				} else {
					JOptionPane.showMessageDialog(null, "Please choose your answer again");
				}
			}
		});
		btnCheck.setBounds(195, 320, 85, 30);
		contentPane.add(btnCheck);
		
		btnGroup = new ButtonGroup();
		btnGroup.add(rdAnswerA);
		btnGroup.add(rdAnswerB);
		btnGroup.add(rdAnswerC);
		btnGroup.add(rdAnswerD);
		
		Set<String> arrStrKey = hmDictionary.keySet();
		ArrayList<String> lstStrkey = new ArrayList<String>(arrStrKey);
		int itemIndex = new Random().nextInt(arrStrKey.size());
		int itemIndexRd1 = new Random().nextInt(arrStrKey.size());
		int itemIndexRd2 = new Random().nextInt(arrStrKey.size());
		int itemIndexRd3 = new Random().nextInt(arrStrKey.size());
		int rdCorrent = new Random().nextInt(4)+1;
		String itemKey = lstStrkey.get(itemIndex);
		String itemKeyRd1 = lstStrkey.get(itemIndexRd1);
		String itemKeyRd2 = lstStrkey.get(itemIndexRd2);
		String itemKeyRd3 = lstStrkey.get(itemIndexRd3);
		
		SlangModel slangItem = hmDictionary.get(itemKey);
		SlangModel slangItemRd1 = hmDictionary.get(itemKeyRd1);
		SlangModel slangItemRd2 = hmDictionary.get(itemKeyRd2);
		SlangModel slangItemRd3 = hmDictionary.get(itemKeyRd3);
		
		if (typeGame == 0) {
			answerCorrect = slangItem.getDefintion();
			lblQuestion.setText("What does �" + slangItem.getSlang() + "� definition?");
			if (rdCorrent == 1) {
				rdAnswerA.setText(slangItem.getDefintion());
				rdAnswerB.setText(slangItemRd1.getDefintion());
				rdAnswerC.setText(slangItemRd2.getDefintion());
				rdAnswerD.setText(slangItemRd3.getDefintion());
				
				rdAnswerA.setActionCommand(slangItem.getDefintion());
				rdAnswerB.setActionCommand(slangItemRd1.getDefintion());
				rdAnswerC.setActionCommand(slangItemRd2.getDefintion());
				rdAnswerD.setActionCommand(slangItemRd3.getDefintion());
			} else if (rdCorrent == 2) {
				rdAnswerA.setText(slangItemRd1.getDefintion());
				rdAnswerB.setText(slangItem.getDefintion());
				rdAnswerC.setText(slangItemRd2.getDefintion());
				rdAnswerD.setText(slangItemRd3.getDefintion());
				
				rdAnswerA.setActionCommand(slangItemRd1.getDefintion());
				rdAnswerB.setActionCommand(slangItem.getDefintion());
				rdAnswerC.setActionCommand(slangItemRd2.getDefintion());
				rdAnswerD.setActionCommand(slangItemRd3.getDefintion());
			}else if (rdCorrent == 3) {
				rdAnswerA.setText(slangItemRd1.getDefintion());
				rdAnswerB.setText(slangItemRd2.getDefintion());
				rdAnswerC.setText(slangItem.getDefintion());
				rdAnswerD.setText(slangItemRd3.getDefintion());
				
				rdAnswerA.setActionCommand(slangItemRd1.getDefintion());
				rdAnswerB.setActionCommand(slangItemRd2.getDefintion());
				rdAnswerC.setActionCommand(slangItem.getDefintion());
				rdAnswerD.setActionCommand(slangItemRd3.getDefintion());
			} else {
				rdAnswerA.setText(slangItemRd1.getDefintion());
				rdAnswerB.setText(slangItemRd2.getDefintion());
				rdAnswerC.setText(slangItemRd3.getDefintion());
				rdAnswerD.setText(slangItem.getDefintion());
				
				rdAnswerA.setActionCommand(slangItemRd1.getDefintion());
				rdAnswerB.setActionCommand(slangItemRd2.getDefintion());
				rdAnswerC.setActionCommand(slangItemRd3.getDefintion());
				rdAnswerD.setActionCommand(slangItem.getDefintion());
			}
		} else {
			answerCorrect = slangItem.getSlang();
			lblQuestion.setText("What does �" + slangItem.getDefintion() + "� slang?");
			if (rdCorrent == 1) {
				rdAnswerA.setText(slangItem.getSlang());
				rdAnswerB.setText(slangItemRd1.getSlang());
				rdAnswerC.setText(slangItemRd2.getSlang());
				rdAnswerD.setText(slangItemRd3.getSlang());
				
				rdAnswerA.setActionCommand(slangItem.getSlang());
				rdAnswerB.setActionCommand(slangItemRd1.getSlang());
				rdAnswerC.setActionCommand(slangItemRd2.getSlang());
				rdAnswerD.setActionCommand(slangItemRd3.getSlang());
			} else if (rdCorrent == 2) {
				rdAnswerA.setText(slangItemRd1.getSlang());
				rdAnswerB.setText(slangItem.getSlang());
				rdAnswerC.setText(slangItemRd2.getSlang());
				rdAnswerD.setText(slangItemRd3.getSlang());
				
				rdAnswerA.setActionCommand(slangItemRd1.getSlang());
				rdAnswerB.setActionCommand(slangItem.getSlang());
				rdAnswerC.setActionCommand(slangItemRd2.getSlang());
				rdAnswerD.setActionCommand(slangItemRd3.getSlang());
			}else if (rdCorrent == 3) {
				rdAnswerA.setText(slangItemRd1.getSlang());
				rdAnswerB.setText(slangItemRd2.getSlang());
				rdAnswerC.setText(slangItem.getSlang());
				rdAnswerD.setText(slangItemRd3.getSlang());
				
				rdAnswerA.setActionCommand(slangItemRd1.getSlang());
				rdAnswerB.setActionCommand(slangItemRd2.getSlang());
				rdAnswerC.setActionCommand(slangItem.getSlang());
				rdAnswerD.setActionCommand(slangItemRd3.getSlang());
			} else {
				rdAnswerA.setText(slangItemRd1.getSlang());
				rdAnswerB.setText(slangItemRd2.getSlang());
				rdAnswerC.setText(slangItemRd3.getSlang());
				rdAnswerD.setText(slangItem.getSlang());
				
				rdAnswerA.setActionCommand(slangItemRd1.getSlang());
				rdAnswerB.setActionCommand(slangItemRd2.getSlang());
				rdAnswerC.setActionCommand(slangItemRd3.getSlang());
				rdAnswerD.setActionCommand(slangItem.getSlang());
			}
		}
	}
}
